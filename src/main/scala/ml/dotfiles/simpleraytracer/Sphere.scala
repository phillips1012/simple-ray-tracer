package ml.dotfiles.simpleraytracer

import scala.math
import scala.util.Random

/**
 * @author phillips1012
 */
class Sphere(
    val center: Vector,
    val radius: Double,
    val surface: Surface)
extends Thing {
  /**
   * Radius squared
   */
  private val radius2 = radius * radius
  
  override def intersect(ray: Ray) = {
    val eo = center - ray.start
    val v = eo dot ray.dir
    var dist = 0.0;
    
    if (!(v < 0)) {
      val disc = radius2 - ((eo dot eo) - (v * v))
      dist = if (disc < 0) 0 else v - math.sqrt(disc)
    } 
    
    if (dist == 0) {
      null
    } else {
      new Intersection(
          thing = this,
          ray = ray,
          dist = dist)
    }
  }
  
  override def normal(pos: Vector) = (pos - center).norm  
}
