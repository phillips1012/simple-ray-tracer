package ml.dotfiles.simpleraytracer

import java.awt.image._

/**
 * @author phillips1012
 */
class Scene(
    val camera: Camera,
    val things: List[Thing],
    val lights: List[Light],
    progressCallback: Double => Unit = progress => {}
) {
  /**
   * Screen width
   */
  var width: Int = 512
  
  /**
   * Screen height
   */
  var height: Int = 512
  
  /**
   * Maximum recursion
   */
  var maxDepth: Int = 10
  
  /**
   * Render
   * @param width
   * @param height
   * @param maxDepth maximum reflections
   */
  def render(width: Int = width, height: Int = height, maxDepth: Int = maxDepth) = {
    this.width = width
    this.height = height
    this.maxDepth = maxDepth
    
    val img = new BufferedImage(width + 1, height + 1, BufferedImage.TYPE_INT_RGB)
    
    val xratio = 1.0 + (height.toFloat / width.toFloat)
    val yratio = 1.0 + (width.toFloat / height.toFloat)

    val total: Double = width * height
    var pixel = 0
    var last = -1
    
    for (y <- 1 to height) for (x <- 1 to width) {
      pixel += 1

      val percent = ((pixel.toFloat / total) * 100).toInt
      if (last != percent) {
        last = percent
        progressCallback(pixel.toDouble / total)
      }
      
      val color = traceRay(new Ray(camera.pos, getPoint(x, y, xratio, yratio)), 0)
      img.setRGB(x, y, color.binary)
    }
    
    img
  }
  
  /**
   * Convert camera coordinate to vector
   * @param x
   * @param y
   */
  private def getPoint(x: Double, y: Double, xratio: Double, yratio: Double) = {
    val x1 = (x - (width / 2.0)) / xratio / width
    val y1 = -(y - (height / 2.0)) / yratio / height
    
    (camera.forward + (camera.right * x1) + (camera.up * y1)).norm
  }
  
  /**
   * Shade intersection
   * @param isect intersection
   * @param depth
   */
  private def shade(isect: Intersection, depth: Int): Color = {
    val d = isect.ray.dir
    val pos = (isect.ray.dir * isect.dist) + isect.ray.start
    val normal = isect.thing.normal(pos)
    val reflectDir = d - (normal* 2 * (normal dot d))
    
    var accum = Color.default
    accum = accum + naturalColor(
        thing = isect.thing,
        pos = pos,
        norm = normal,
        rd = reflectDir)
    
    if (depth >= maxDepth) {
      accum + new Color(0.5, 0.5, 0.5)
    } else {
      accum + reflectionColor(
          thing = isect.thing,
          pos = pos + (reflectDir * 0.001),
          norm = normal,
          rd = reflectDir,
          depth = depth)
    }
  }
  
  /**
   * Get reflection color
   * @param thing
   * @param pos position
   * @param norm
   * @param rd reflection direction
   * @param depth
   */
  private def reflectionColor(thing: Thing, pos: Vector,
                              norm: Vector, rd: Vector, depth: Int) =
    traceRay(new Ray(pos, rd), depth + 1) * thing.surface.reflect(pos)
  
  /**
   * Get natural color
   * @param thing
   * @param pos position
   * @param norm
   * @param rd reflection direction
   */
  private def naturalColor(thing: Thing, pos: Vector,
                           norm: Vector, rd: Vector) = {
    var accum = Color.default
    
    for (light <- lights) {
      val ldis = light.pos - pos
      val livec = ldis.norm
      
      val neatIsect = testRay(new Ray(pos, livec))
      val isInShadow = !((neatIsect > ldis.mag) || (neatIsect == 0))
      
      if (!isInShadow) {
        val illum = livec dot norm
        val lcolor = if (illum > 0) light.color * illum else Color.default
        
        val specular = livec dot rd.norm
        val scolor = if (specular > 0) {
          light.color * math.pow(specular, thing.surface.roughness)
        } else {
          Color.default
        }
        
        accum = accum + (thing.surface.diffuse(pos) * lcolor) +
                        (thing.surface.specular(pos) * scolor)
      }
    }
    
    accum
  }
  
  /**
   * Trace ray for color
   * @param ray ray to trace
   * @param depth
   */
  private def traceRay(ray: Ray, depth: Int) = {
    val isects = intersections(ray)
    if (isects.length == 0) Color.default
    else shade(isects.head, depth)
  }
  
  /**
   * Get distance of nearest intersection
   */
  private def testRay(ray: Ray) = {
    val isects = intersections(ray)
    if (isects.length == 0) 0.0
    else isects.head.dist
  }
  
  /**
   * Get intersections and sort by distance
   * @param ray ray to check
   */
  private def intersections(ray: Ray) =
    things.map(thing => thing intersect ray)
      .filter(thing => thing != null)
      .sortBy(isect => isect.dist)
}
