package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 */
class Plane(
    val norm: Vector,
    val offset: Double,
    val surface: Surface)
extends Thing {
  override def intersect(ray: Ray) = {
    val denom = norm dot ray.dir
    if (denom > 0) null
    else new Intersection(
        thing = this,
        ray = ray,
        dist = ((norm dot ray.start) + offset) / -(denom))
  }
    
  override def normal(pos: Vector) = norm
}
