package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 * @param pos position
 * @param lookAt look at
 */
class Camera(val pos: Vector, lookAt: Vector) {
  val forward = (lookAt - pos).norm
  val down = new Vector(0.0, -1.0, 0.0)
  val right = (forward cross down).norm * 1.5
  val up = (forward cross right).norm * 1.5
}