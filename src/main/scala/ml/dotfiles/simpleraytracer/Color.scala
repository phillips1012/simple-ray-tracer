package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 */
case class Color(r: Double, g: Double, b: Double) {
  /**
   * Multiply two colors
   */
  def *(right: Color) =
    new Color(r * right.r, g * right.g, b * right.b)
  
  /**
   * Scale color
   */
  def *(right: Double) =
    new Color(r * right, g * right, b * right)
  
  /**
   * Add two colors
   */
  def +(right: Color) =
    new Color(r + right.r, g + right.g, b + right.b)
  
  /**
   * Subtract two colors
   */
  def -(right: Color) =
    new Color(r - right.r, g - right.g, b - right.b)
  
  def binary = {
    val newR = 0 max math.floor(r * 255).toInt min 255
    val newG = 0 max math.floor(g * 255).toInt min 255
    val newB = 0 max math.floor(b * 255).toInt min 255
    
    (newR << 16) | (newG << 8) | (newB)
  }
}

object Color {
  /**
   * Default color
   */
  val default = new Color(0.0, 0.0, 0.0)
}
