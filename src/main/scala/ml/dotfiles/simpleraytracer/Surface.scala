package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 * @param roughness
 * @param diffuse diffuse method
 * @param specular specular method
 * @param reflect reflect method
 */
case class Surface(
    roughness: Double,
    diffuse: Vector => Color,
    specular: Vector => Color,
    reflect: Vector => Double)

object Surface {
  /**
   * Checkerboard surface
   */
  val checkerboard = new Surface(
    roughness = 150,
    diffuse = pos => if ((math.floor(pos.z) + math.floor(pos.x)) % 2 != 0)
      new Color(1.0, 1.0, 1.0) else new Color(0.0, 0.0, 0.0),
    specular = pos => new Color(0.0, 0.0, 0.0),
    reflect = pos => if ((math.floor(pos.z) + math.floor(pos.x)) % 2 != 0)
      0.1 else 0.7
  )
  
  /**
   * Shiny surface
   */
  val shiny = new Surface(
    roughness = 50,
    diffuse = pos => new Color(1.0, 1.0, 1.0),
    specular = pos => new Color(0.5, 0.5, 0.5),
    reflect = pos => 0.6
  )
}
