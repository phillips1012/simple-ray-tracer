package ml.dotfiles.simpleraytracer

import java.io.File
import javax.imageio.ImageIO

/**
 * @author phillips1012
 */
object Main extends App {
  val scene = new Scene(
      camera = new Camera(
        new Vector(3.0, 2.0, 4.0), new Vector(-1.0, 0.5, 0.0)),

      things = List(
          new Plane(
              norm = new Vector(0.0, 1.0, 0.0),
              offset = 0.0,
              surface = Surface.checkerboard),
          new Sphere(
              center = new Vector(0.0, 1.0, 0.0),
              radius = 1.0,
              surface = Surface.shiny),
          new Sphere(
              center = new Vector(-1.0, 0.5, 1.5),
              radius = 0.5,
              surface = Surface.shiny)),
    
      lights = List(
          new Light(
              new Vector(0.0, 10, 0.0),
              new Color(0.49, 0.07, 0.07)),
          new Light(
              new Vector(1.5,2.5,1.5),
              new Color(.07,.07,.49)),
          new Light(
              new Vector(1.5,2.5,-1.5),
              new Color(.07,.49,.071)),
          new Light(
              new Vector(0,3.5,0),
              new Color(.21,.21,.35)
      )),

      progressCallback = progress =>
        println(s"${(100 * progress).toInt}")
  )
  
  
  val out = new File("out.png")
  ImageIO.write(scene.render(8192, 8192, 16), "png", out)
}
