package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 */
trait Thing {
  /**
   * Surface
   */
  val surface: Surface
  
  /**
   * Intersect method
   */
  def intersect(ray: Ray): Intersection
  
  /**
   * Normal method
   */
  def normal(pos: Vector): Vector
}