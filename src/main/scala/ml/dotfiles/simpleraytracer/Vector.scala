package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 */
case class Vector(x: Double, y: Double, z: Double) {
  /**
   * Multiply a vector by a number
   */
  def *(right: Double) =
    new Vector(x * right, y * right, z * right)
  
  /**
   * Multiply a vector by a vector
   */
  def *(right: Vector) =
    new Vector(x * right.x, y * right.y, z * right.z)
  
  /**
   * Subtract vectors
   */
  def -(right: Vector) =
    new Vector(x - right.x, y - right.y, z - right.z)
  
  /**
   * Add vectors
   */
  def +(right: Vector) =
    new Vector(x + right.x, y + right.y, z + right.z)
  
  /**
   * Dot product
   */
  def dot(right: Vector) =
    (x * right.x) + (y * right.y) + (z * right.z)
  
  /**
   * Magnitude
   */
  def mag = math.sqrt(this dot this)
  
  /**
   * Norm
   */
  def norm = * (1.0 / mag)
  
  /**
   * Cross product
   */
  def cross(right: Vector) =
    new Vector((y * right.z) - (z * right.y),
               (z * right.x) - (x * right.z),
               (x * right.y) - (y * right.x));

  /**
   * Check equality
   */
  def equals(right: Vector) =
    (x == right.x) && (y == right.y) && (z == right.z)
}
