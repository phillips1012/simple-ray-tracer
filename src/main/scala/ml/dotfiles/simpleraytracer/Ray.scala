package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 * @param start start
 * @param dir direction
 */
case class Ray(
    start: Vector,
    dir: Vector)
