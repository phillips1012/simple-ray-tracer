package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 * @param pos position
 * @param color
 */
case class Light(
    pos: Vector,
    color: Color)
