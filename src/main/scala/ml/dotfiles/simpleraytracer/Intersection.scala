package ml.dotfiles.simpleraytracer

/**
 * @author phillips1012
 * @param thing
 * @param ray
 * @param dist distance
 */
case class Intersection(
    thing: Thing,
    ray: Ray,
    dist: Double)
